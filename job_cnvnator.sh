#!/bin/sh
echo "Start - `date`"
#$ -N CNV
#$ -q all.q
#$ -l h_rt=220:00:00
#$ -pe smp 30
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu
# https://sites.google.com/a/brown.edu/bioinformatics-in-biomed/cnvnator
##########################
# Predicting CNV regions #
##########################
# Chromosome names and lengths are parsed from sam/bam file header. 
# One can override this default behavior by using the -genome option
# https://github.com/abyzovlab/CNVnator

bin_size=10000

# out.root  -- output ROOT file. See ROOT package documentation.

# Chromosome names must be specified the same way as they are described in
# sam/bam header, e.g., chrX or X. One can specify multiple chromosomes separated by
# space. If no chromosome is specified, read mapping is extracted for all chromosomes
# in sam/bam file.

# ROOT 
source /home/adinasarapu/root/bin/thisroot.sh
PROJ_DIR="$HOME/zwick_rare/geisert"
# BWAIndex, WholeGenomeFasta
REF_FILE="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/BWAIndex/genome.fa"
MAP_DIR="$HOME/zwick_rare/geisert/BWA"
CHR_DIR="$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/Chromosomes"

module load bcftools/1.3
module load snpEff/4.2
module load annovar/201602

# STEP 1: EXTRACTING READ MAPPING FROM BAM/SAM FILES

:<<'END'
cnvnator -root NA12878.root -chrom 1 2 3 -tree NA12878_ali.bam
for bam files with a header like this:
	@HD VN:1.4    GO:none  SO:coordinate
	@SQ SN:1      LN:249250621
	@SQ SN:2      LN:243199373
	@SQ SN:3      LN:198022430
cnvnator -root NA12878.root -chrom chr1 chr2 chr3 -tree NA12878_ali.bam
for bam files with a header like this:
	@HD VN:1.4    GO:none  SO:coordinate
	@SQ SN:chr1   LN:249250621
	@SQ SN:chr2   LN:243199373
	@SQ SN:chr3   LN:198022430
END
# -chrom chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 \
for i in {6..9}; do
	SID="SL14857${i}"
	OUT_DIR="${PROJ_DIR}/cnvnator_bin_size${bin_size}/${SID}"

	if [ ! -d ${OUT_DIR} ]; then
		mkdir -p ${OUT_DIR}/{plots,annovar,snpEff}
	fi
	#
	cnvnator \
		-root ${OUT_DIR}/${SID}.hg38.root \
		-unique \
		-tree \
		${MAP_DIR}/${SID}/${SID}.dedupe.bam

	cnvnator \
		-root ${OUT_DIR}/${SID}.hg38.root \
		-d ${CHR_DIR} \
		-his ${bin_size}

	# STEP 2: CALCULATING STATISTICS

	# This step must be completed before proceeding to partitioning 
	# and CNV calling.
	cnvnator \
		-root ${OUT_DIR}/${SID}.hg38.root \
		-stat ${bin_size} 

	# STEP 3: RD SIGNAL PARTITIONING

	# Option -ngc specifies not to use GC corrected RD signal. 
	# Partitioning is the most time consuming step.
	cnvnator \
		-root ${OUT_DIR}/${SID}.hg38.root \
		-partition ${bin_size} -ngc

	# STEP 4: CNV CALLING

	#The output is as follows:

	#CNV_type coordinates CNV_size normalized_RD e-val1 e-val2 e-val3 e-val4 q0

	#normalized_RD -- normalized to 1.
	#e-val1        -- is calculated using t-test statistics.
	#e-val2        -- is from the probability of RD values within the region to be
	#				in the tails of a gaussian distribution describing frequencies of RD values in
	#				bins.
	#e-val3        -- same as e-val1 but for the middle of CNV
	#e-val4        -- same as e-val2 but for the middle of CNV
	#q0            -- fraction of reads mapped with q0 quality
	#
	#To have correct output of q0 field one needs to use the option -unique when
	#extracting read mapping from bam/sam files.
	# -chrom chr1 
	cnvnator \
		-root ${OUT_DIR}/${SID}.hg38.root \
		-call ${bin_size} -ngc \
		> ${OUT_DIR}/${SID}.hg38_CNV.txt

	# Setp 12. Convert the CNV file into VCF format. This is optional.
	$HOME/CNVnator_v0.3.2/cnvnator2VCF.pl \
		${OUT_DIR}/${SID}.hg38_CNV.txt \
		${CHR_DIR} \
		> ${OUT_DIR}/${SID}.hg38_CNV.vcf

	# bcftools: stats
	# Extract stats from a VCF/BCF file or compare two VCF/BCF files
	# -F faidx indexed reference sequence file (genome.fa.fai) to determine INDEL context
	# -s samples List
	bcftools stats \
		-F ${REF_FILE}/genome.fa \
		-s - ${OUT_DIR}/${SID}.hg38_CNV.vcf \
		> ${OUT_DIR}/${SID}.hg38_vcf_stats

	# bcftools: plot-vcfstats
	# -p The output directory. This directory will be created if it does not exist.
	plot-vcfstats \
		-p ${OUT_DIR}/plots/ ${OUT_DIR}/${SID}.hg38_vcf_stats

	#Annotate a VCF with snpEff
	$SNPEFF -v \
		-stats ${OUT_DIR}/snpEff/${SID}.hg38_CNV.snpEff.html hg38 \
		${OUT_DIR}/${SID}.hg38_CNV.vcf \
		> ${OUT_DIR}/snpEff/${SID}.hg38_CNV.snpEff.vcf

	# Annotate a VCF with annovar
	convert2annovar.pl \
		-format vcf4 ${OUT_DIR}/${SID}.hg38_CNV.vcf \
		> ${OUT_DIR}/annovar/${SID}.hg38_CNV.avinput

	annotate_variation.pl \
		-out ${OUT_DIR}/annovar/${SID}.hg38_CNV \
		-build hg38 \
		${OUT_DIR}/annovar/${SID}.hg38_CNV.avinput \
		/home/adinasarapu/humandb/ \
		-dbtype refGene
# > ${OUT_DIR}/${SID}.hg38_NFkB.txt
	cnvnator -root ${OUT_DIR}/${SID}.hg38.root -genotype ${bin_size} \
		> ${OUT_DIR}/${SID}.hg38_NFkB.txt << EOF 
	chr1:205043212-205078043
	chr2:60881495-60928156
	chr4:102501329-102617302
	chr6:3076824-3115187
	chr7:27739373-27829767
	chr10:102394110-102402529
	chr11:65653596-65662972
	chr16:50742050-50801935
	chr18:62325287-62387710
	chr18:9475532-9538108
	chr19:45001449-45038198
	chrX:154542244-154565046 
	exit 
EOF
done
module unload bcftools/1.3
module unload snpEff/4.2
module unload annovar/201602

# STEP 5: VISUALIZING SPECIFIED REGIONS
# cnvnator -root file.root [-chrom chr_name1 ...] -view bin_size [-ngc]

# STEP 6: Genotyping of multiple regions one can use input piping, e.g.,
# For efficient genotype calculations, we recommend that you sort the list of
# regions by chromosomes.
